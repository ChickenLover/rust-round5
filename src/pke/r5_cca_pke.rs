use crate::types::Random;
use crate::parameters::Parameters;
use super::r5_dem::{round5_dem, round5_dem_inverse};
use crate::r5_cca_kem::{r5_cca_kem_keygen, r5_cca_kem_encapsulate, r5_cca_kem_decapsulate};


pub fn r5_cca_pke_keygen(pk: &mut [u8], sk: &mut [u8], drbg: &mut dyn Random, params: &Parameters) {
    r5_cca_kem_keygen(pk, sk, drbg, params);
}

pub fn r5_cca_pke_encrypt(ct: &mut [u8], msg: &[u8], pk: &[u8], drbg: &mut dyn Random, params: &Parameters) {
    let mut c1 = vec![0u8; params.ct_size as usize + params.kappa_bytes as usize];
    let mut k = vec![0u8; params.kappa_bytes as usize];
    r5_cca_kem_encapsulate(&mut c1, &mut k, &pk, drbg, params);
    
    ct[..c1.len()].copy_from_slice(&c1);

    round5_dem(&mut ct[c1.len()..], &k, &msg, params);
}

pub fn r5_cca_pke_decrypt(ct: &[u8], sk: &[u8], params: &Parameters) -> Vec<u8> {
    let mut k = vec![0u8; params.kappa_bytes as usize];
    r5_cca_kem_decapsulate(&mut k, &ct[..params.ct_size as usize + params.kappa_bytes as usize], sk, params);
    round5_dem_inverse(&k, &ct[params.ct_size as usize + params.kappa_bytes as usize..], params)
}
