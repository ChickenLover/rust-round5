mod r5_cca_pke;
mod r5_dem;

use crate::types::Random;
use crate::parameters::Parameters;
use crate::pke::r5_cca_pke::{r5_cca_pke_keygen, r5_cca_pke_encrypt, r5_cca_pke_decrypt};


pub struct R5Pke {
    params: Parameters,
    pk: Option<Vec<u8>>,
    sk: Option<Vec<u8>>
}


impl R5Pke {
    pub fn new(params: Parameters) -> R5Pke {
        R5Pke { params, pk: None, sk: None }
    }

    pub fn default() -> R5Pke {
        let params = Parameters::r5n1_1_0d_pke();
        R5Pke { params, pk: None, sk: None }
    }

    pub fn public(&self) -> &[u8] {
        self.pk.as_ref().expect("You should call .keypair() before you take the pubkey")
    }

    pub fn keypair(&mut self, rng: &mut dyn  Random) {
        self.pk = Some(vec![0u8; self.params.c_pk]);
        self.sk = Some(vec![0u8; self.params.c_sk]);
        r5_cca_pke_keygen(self.pk.as_mut().unwrap(), self.sk.as_mut().unwrap(), rng, &self.params);
    }
    
    pub fn encrypt(&self, msg: &[u8], pubkey: &[u8], rng: &mut dyn Random) -> Vec<u8> {
        let mut ct = vec![0u8; msg.len() + self.params.c_b];
        r5_cca_pke_encrypt(&mut ct, msg, pubkey, rng, &self.params);
        ct
    }

    pub fn open(&self, ciphertext: &[u8]) -> Vec<u8> {
        r5_cca_pke_decrypt(ciphertext, self.sk.as_ref().unwrap(), &self.params)
    }
}
