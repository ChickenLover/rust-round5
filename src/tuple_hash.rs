// TO-DO implement AES_DRBG

use crate::EMPTY;
use crate::parameters::Parameters;
use tiny_keccak::{Xof, Hasher, TupleHash};
use safe_transmute::to_bytes::transmute_to_bytes_mut;

#[inline]
pub fn u8_to_u16(a: &[u8]) -> u16 {
    ((a[1] as u16) << 8) + a[0] as u16
}

pub fn r5_tuple_hash(output: &mut [u8], domain: &[u8], first: &[u8], second: &[u8], params: &Parameters) {
    let mut hasher = if params.kappa_bytes > 16 {
        TupleHash::v256(&EMPTY)
    } else {
        TupleHash::v128(&EMPTY)
    };

    hasher.update(domain);
    if first.len() > 0 {
        hasher.update(first);
    }
    if second.len() > 0 {
        hasher.update(second);
    }
    hasher.finalize(output);
}


pub struct R5TupleHasher {
    hasher: TupleHash
}

impl R5TupleHasher {
    pub fn new(domain: &[u8], first: Option<&[u8]>, second: Option<&[u8]>, params: &Parameters) -> R5TupleHasher {
        let mut hasher = if params.kappa_bytes > 16 {
            TupleHash::v256(&EMPTY)
        } else {
            TupleHash::v128(&EMPTY)
        };
        hasher.update(domain);
        match first {
            Some(data) => { hasher.update(data) },
            None => {  },
        };
        match second {
            Some(data) => { hasher.update(data) },
            None => {  },
        };
        hasher.finalize(&mut EMPTY);
        R5TupleHasher { hasher }
    }

    pub fn hash16(output: &mut [u16], domain: &[u8], first: &[u8], second: &[u8], params: &Parameters) {
        let out = transmute_to_bytes_mut(output);
        r5_tuple_hash(out, domain, first, second, params);
    }

    pub fn squeeze(&mut self, output: &mut [u8]) {
        self.hasher.squeeze(output);
    }

    pub fn squeeze16(&mut self) -> u16 {
        let mut rnd: [u8; 2] = [0, 0];
        self.squeeze(&mut rnd);
        u8_to_u16(&rnd)
    }
}
