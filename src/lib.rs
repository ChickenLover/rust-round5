//#![feature(test)]
//extern crate test;

use num::{Integer, NumCast};

#[inline]
pub(crate) fn bits_to_bytes<T: NumCast + Integer>(a: T) -> T {
    a.div_ceil(&NumCast::from(8).unwrap())
}

pub(crate) const NBLOCKS: usize = 8;
pub(crate) const EMPTY: [u8; 0] = [];

mod tuple_hash;
mod pack;
mod r5_core;
mod r5_cpa_hash;
#[cfg(any(feature="cca-kem",
           feature="cca-pke"
))]
mod r5_cca_hash;
mod r5_cpa_pke;
#[cfg(any(feature="cca-kem",
           feature="cca-pke"
))]
mod r5_cca_kem;
mod xef;

pub mod types;
pub mod nist_rng;
pub mod parameters;

#[cfg(any(feature = "cpa-kem",
          feature = "cca-kem"
))]
pub mod kem;

#[cfg(feature = "cca-pke")]
pub mod pke;
