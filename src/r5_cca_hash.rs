use crate::EMPTY;
use crate::parameters::Parameters;
use crate::tuple_hash::r5_tuple_hash;

static HCCAKEM_DOMAIN: &'static [u8; 7] = b"HCCAKEM";
static GCCAKEM_DOMAIN: &'static [u8; 7] = b"GCCAKEM";
static HR5DEM_DOMAIN: &'static [u8; 6] = b"HR5DEM";

pub fn hccakem(output: &mut [u8], first: &[u8], second: &[u8], params: &Parameters) {
    r5_tuple_hash(output, HCCAKEM_DOMAIN, first, second, params);
}

pub fn gccakem(output: &mut [u8], first: &[u8], second: &[u8], params: &Parameters) {
    r5_tuple_hash(output, GCCAKEM_DOMAIN, first, second, params);
}

pub fn hashr5dem(output: &mut [u8], first: &[u8], params: &Parameters) {
    r5_tuple_hash(output, HR5DEM_DOMAIN, first, &EMPTY, params);
}
