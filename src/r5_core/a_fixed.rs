use once_cell::sync::OnceCell;
use crate::parameters::Parameters;
use crate::r5_core::a_random::create_a_random;


pub static A_FIXED: OnceCell<Vec<u16>> = OnceCell::new();

#[allow(dead_code)]
pub fn create_a_fixed(sigma: &[u8], params: &Parameters) -> Result<(), Vec<u16>> {
    let mut a = create_a_random(sigma, params);
    for e in a.iter_mut() {
        *e &= (params.q - 1) as u16;
    }
    A_FIXED.set(a)
}

#[allow(dead_code)]
pub fn get_a_fixed() -> &'static Vec<u16> {
    A_FIXED.get().unwrap()
}
