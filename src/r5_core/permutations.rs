use crate::parameters::Parameters;
use crate::tuple_hash::R5TupleHasher;


const A_PERMUTATION_DOMAIN: &'static [u8; 12] = b"APermutation";

pub fn permutation_tau_1(seed: &[u8], params: &Parameters) -> Vec<u32> {
    let range_divisor = 0x10000 / params.d as u32;
    let range_limit = params.d as u32 * range_divisor;
    let mut hasher = R5TupleHasher::new(A_PERMUTATION_DOMAIN, Some(seed), None, params);
    let mut perm: Vec<u32> = Vec::with_capacity(params.d as usize);
    let mut rnd: u16;
    for i in 0..params.d as u32 {
        while {
            rnd = hasher.squeeze16();
            rnd >= range_limit as u16
        } {};
        rnd = rnd / range_divisor as u16;
        perm.push(i * params.d as u32 + rnd as u32);
    }
    perm
}


pub fn permutation_tau_2(seed: &[u8], params: &Parameters) -> Vec<u32> {
    let mut hasher = R5TupleHasher::new(A_PERMUTATION_DOMAIN, Some(seed), None, params);
    let mut perm: Vec<u32> = Vec::with_capacity(params.d as usize);
    let mut rnd: u16;
    let mut v = vec![0u8; params.tau2_len as usize];
    for _ in 0u32..params.k as u32 {
        while {
            rnd = (hasher.squeeze16() as u32 & (params.tau2_len - 1)) as u16;
            v[rnd as usize] != 0
        } {};
        v[rnd as usize] = 1;
        perm.push(rnd as u32);
    }
    perm
}
