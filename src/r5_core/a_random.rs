use crate::NBLOCKS;
use crate::parameters::Parameters;
use crate::tuple_hash::R5TupleHasher;


const A_GENERATION_DOMAIN: &'static [u8; 4] = b"AGEN";

pub fn create_a_random(seed: &[u8], params: &Parameters) -> Vec<u16> {
    let mut a_random = vec![0u16; params.tau2_len as usize];
    let block_len = match params.tau {
        2 => {
            (params.tau2_len as usize + NBLOCKS - 1) / NBLOCKS
        },
        _ => {
            (params.d as usize + NBLOCKS - 1) / NBLOCKS
        }
    };
    for i in 0..NBLOCKS {
        R5TupleHasher::hash16(&mut a_random[i * block_len..(i + 1) * block_len], A_GENERATION_DOMAIN, seed, &[i as u8], params);
    }
    a_random
}
