use crate::types::Random;
use crate::r5_cpa_hash::hcpakem;
use crate::parameters::Parameters;
use crate::r5_cpa_pke::{r5_cpa_pke_keygen, r5_cpa_pke_encrypt, r5_cpa_pke_decrypt};


pub fn r5_cpa_kem_keygen(pk: &mut [u8], sk: &mut [u8], drbg: &mut dyn Random, params: &Parameters) {
    r5_cpa_pke_keygen(pk, sk, drbg, params);
}

pub fn r5_cpa_kem_encapsulate(pk: &[u8], drbg: &mut dyn Random, shared_secret_out: &mut [u8], ciphertext_out: &mut [u8], params: &Parameters) {
    let mut m = vec![0u8; params.kappa_bytes as usize];
    drbg.fill_bytes(&mut m);

    let mut rho = vec![0u8; params.kappa_bytes as usize];
    drbg.fill_bytes(&mut rho);
    
    r5_cpa_pke_encrypt(ciphertext_out, pk, &m, &rho, params);

    //println!("m: {:?}", m);
    //println!("rho: {:?}", rho);
    //println!("ciphertext_out: {:?}", ciphertext_out);
    hcpakem(shared_secret_out, &m, ciphertext_out, params);

}

pub fn r5_cpa_kem_decapsulate(ct: &[u8], sk: &[u8], shared_secret_out: &mut [u8], params: &Parameters) {
    let mut m = vec![0u8; params.kappa_bytes as usize];

    r5_cpa_pke_decrypt(&mut m, sk, ct, params);

    hcpakem(shared_secret_out, &m, ct, params);
}
