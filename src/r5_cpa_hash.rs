use crate::parameters::Parameters;
use crate::tuple_hash::r5_tuple_hash;

static HCPAKEM_DOMAIN: &'static [u8; 7] = b"HCPAKEM";

pub fn hcpakem(output: &mut [u8], first: &[u8], second: &[u8], params: &Parameters) {
    r5_tuple_hash(output, HCPAKEM_DOMAIN, first, second, params);
}
