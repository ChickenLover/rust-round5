#[cfg(not(any(
    feature = "cca-pke",
    feature = "cpa-kem",
    feature = "cca-kem"
)))]
compile_error!(
    "You need to specify at least one cryptographic protocol you intend to use. \
    Available options:\n\
    pke, kem\n\
    e.g.\n\
    round5 = { version = \"0.1.1\", features = [\"kem\"] }"
);

fn main() {
}
