#![cfg(feature="cca-kem")]
use hex::FromHex;
use std::fs::File;
use std::error::Error;
use std::io::{self, BufRead};
use std::path::{PathBuf, Path};
use round5::nist_rng::{Aes256CtrDrbg, CUSTOMIZATION_STRING_LENGTH};
use round5::kem::R5Kem;
use round5::parameters::Parameters;

static KEM_KATS_FOLDER: &'static str = "tests/NIST/KEM/";
static R5N1_1CCA_0d_PATH: &'static str = "R5N1_1CCA_0d/PQCkemKAT_5772.rsp";
static R5N1_1CPA_0d_PATH: &'static str = "R5N1_1CPA_0d/PQCkemKAT_16.rsp";

#[test]
fn r5n1_1cpa_0d_kem() -> Result<(), Box<dyn Error>> {
    let path = build_path(R5N1_1CPA_0d_PATH);
    let parameters = Parameters::r5n1_1cpa_0d_kem();
    test_kem(parameters, path)
}

/*
#[test]
fn r5n1_1cca_0d_kem() -> Result<(), Box<dyn Error>> {
    let path = build_path(R5N1_1CCA_0d_PATH);
    let parameters = Parameters::r5n1_1cca_0d_kem();
    test_kem(parameters, path)
}*/

#[allow(unused_variables)]
fn test_kem(params: Parameters, path: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut reader = read_lines(&path.as_path())?.skip(2);

    let count: usize = 10;
    for i in 0..count {
        println!("#{} test:", i);
        reader.next();

        let seed_str = &reader.next().unwrap()?[7..];
        let seed = <[u8; CUSTOMIZATION_STRING_LENGTH]>::from_hex(seed_str)?;

        let test_pk = hex::decode(&reader.next().unwrap()?[5..])?;
        let test_sk = hex::decode(&reader.next().unwrap()?[5..])?;

        let test_ct = hex::decode(&reader.next().unwrap()?[5..])?;
        let test_ss = hex::decode(&reader.next().unwrap()?[5..])?;
        reader.next();

        let mut aes_drbg: Aes256CtrDrbg = Aes256CtrDrbg::new(&seed, None);
    
        let mut kem = R5Kem::new(params);
        kem.keypair(&mut aes_drbg);
        assert!(test_pk == kem.public().to_vec());
        println!("Keys are all good!");

        let mut ct: Vec<u8> = vec![0u8; kem.params.ct_size as usize];
        let mut ss: Vec<u8> = vec![0u8; kem.params.c_b];
        kem.enc(kem.public(), &mut aes_drbg, &mut ss, &mut ct);
        assert!(ct == test_ct);
        println!("encrypted messages are equal!");

        kem.dec(&ct, &mut ss);
        assert!(ss == test_ss);
        println!("decrypted messages are equal!");
    }
    Ok(())
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn build_path(name: &str) -> PathBuf {
    let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    path.push(KEM_KATS_FOLDER);
    path.push(name);
    path
}
