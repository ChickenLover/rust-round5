#![cfg(feature="cca-pke")]
use hex::FromHex;
use std::fs::File;
use std::error::Error;
use std::io::{self, BufRead};
use std::path::{PathBuf, Path};
use round5::nist_rng::{Aes256CtrDrbg, CUSTOMIZATION_STRING_LENGTH};
use round5::pke::R5Pke;
use round5::parameters::Parameters;

static PKE_KATS_FOLDER: &'static str = "tests/NIST/PKE/";
static R5N1_1CCA_0d_PATH: &'static str = "R5N1_1CCA_0d/PQCencryptKAT_5772.rsp";


#[test]
fn r5n1_1cca_0d_kem() -> Result<(), Box<dyn Error>> {
    let path = build_path(R5N1_1CCA_0d_PATH);
    let parameters = Parameters::r5n1_1cca_0d_kem();
    test_pke(parameters, path)
}


#[allow(unused_variables)]
fn test_pke(params: Parameters, path: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut reader = read_lines(&path.as_path())?.skip(2);

    let count: usize = 10;
    for i in 0..count {
        println!("#{} test:", i);
        reader.next();

        let seed_str = &reader.next().unwrap().unwrap()[7..];
        let seed = <[u8; CUSTOMIZATION_STRING_LENGTH]>::from_hex(seed_str).unwrap();

        let mlen = &reader.next().unwrap().unwrap()[7..].parse::<usize>().unwrap();
        let msg = hex::decode(&reader.next().unwrap().unwrap()[6..]).unwrap();

        let test_pk = hex::decode(&reader.next().unwrap().unwrap()[5..]).unwrap();
        let test_sk = hex::decode(&reader.next().unwrap().unwrap()[5..]).unwrap();

        let clen = &reader.next().unwrap().unwrap()[7..].parse::<usize>().unwrap();
        let test_c = hex::decode(&reader.next().unwrap().unwrap()[4..]).unwrap();

        reader.next();
        let mut aes_drbg: Aes256CtrDrbg = Aes256CtrDrbg::new(&seed, None);
        let mut pke = R5Pke::new(params);
        pke.keypair(&mut aes_drbg);
        //assert!(test_pk == pke.public().to_vec());
        println!("Keys are all good!");

        let c = pke.encrypt(&msg, pke.public(), &mut aes_drbg);
        //assert!(c == test_c);
        println!("encrypted messages are equal!");

        let m = pke.open(&c);
        //assert!(m == msg);
        println!("decrypted messages are equal!");
    }
    Ok(())
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn build_path(name: &str) -> PathBuf {
    let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    path.push(PKE_KATS_FOLDER);
    path.push(name);
    path
}
