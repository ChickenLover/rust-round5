#![no_main]
use std::{fmt, cmp};
use libfuzzer_sys::fuzz_target;
use arbitrary::{Arbitrary, Result, Unstructured};

use round5::kem::R5Kem;
use round5::nist_rng::{Aes256CtrDrbg, CUSTOMIZATION_STRING_LENGTH};


#[derive(Clone)]
pub struct FuzzData {
    seed: [u8; CUSTOMIZATION_STRING_LENGTH]
}

impl fmt::Debug for FuzzData {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.seed[..].fmt(formatter)
    }
}

impl Arbitrary for FuzzData {
    fn arbitrary(u: &mut Unstructured<'_>) -> Result<Self> {
        let len = u.arbitrary_len::<u8>()?;
        let mut fuzz_data = [0u8; CUSTOMIZATION_STRING_LENGTH];
        let end = cmp::min(len, CUSTOMIZATION_STRING_LENGTH);
        for i in 0..end {
            fuzz_data[i] = u8::arbitrary(u)?;
        }
        Ok(FuzzData{ seed: fuzz_data })
    }
}

fuzz_target!(|data: FuzzData| {
    let mut aes_drbg: Aes256CtrDrbg = Aes256CtrDrbg::new(&data.seed, None);
    let mut kem = R5Kem::default();
    kem.keypair(&mut aes_drbg);
    let (test_ss, ct) = kem.enc(kem.public(), &mut aes_drbg);
    let ss = kem.dec(&ct);
    assert!(ss == test_ss);
});
